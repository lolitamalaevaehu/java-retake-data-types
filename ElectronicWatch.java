package com.epam.rd.autotasks.meetautocode;

import java.util.Scanner;

public class ElectronicWatch {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int seconds = scanner.nextInt();

        seconds %= 86400;

        int hours = seconds / 3600;
        int remainingSeconds = seconds % 3600;
        int minutes = remainingSeconds / 60;
        int extraSeconds = remainingSeconds % 60;

        System.out.printf("%d:%02d:%02d%n", hours, minutes, extraSeconds);

    }
}
